/**
 * The JENTIS consent Borlabs Bridge
 *
 */
window.jentis = window.jentis || {};
window.jentis.vendor = window.jentis.vendor || {};
window.jentis.vendor.cmp_bridge_borlabs = new function () {
    /**
     * Constructor
     *
     */
    this.init = function () {
        //Initial Object Variables
        this.consentEngineReady = false;
        this.borlabsReady = false;

        this.templateConfig = window.jentis.consent.engine.getTemplateConfig();
        this.bridgeConfig = this.templateConfig.bridge;
        this.vendorConfig = this.templateConfig.vendors;

        //Start listening to tje JENTIS Consent Engine and to the API of Borlabs
        this.listenConsentEngine();
        this.listenBorlabs();
        this.listenInitBorlabs();
        this.listenConsentEngineShowAgain();

    };

    /**
     * Listen to the Consent Engine whenever it is ready and we can start to track at the server.
     *
     */
    this.listenConsentEngine = function () {

        if (typeof window.jentis.consent.engine !== "undefined") {
            //If the engine is allready loaded, we maybe missed the events, so we want to register at the engine instead of the document.
            var oEventBaseObject = window.jentis.consent.engine;
        } else {
            //No engine allready exists, so it is safe to register at the document.
            var oEventBaseObject = window.document;
        }

        //Now listen to the Init Event of the Consent Engine.
        (function (oMe, oEventBaseObject) {
            oEventBaseObject.addEventListener('jentis.consent.engine.init', function (e) {
                oMe.consentEngineReady = true;
                oMe.startTracking();
            });

        })(this, oEventBaseObject);

    };

    /**
     * Listen to the Borlabs API whenever it is ready and we can start to track at the server.
     *
     */
    this.listenBorlabs = function () {
        (function (oMe) {
            window.addEventListener('borlabs-cookie-consent-saved', function (e) {
                oMe.borlabsReady = true;
                oMe.startTracking();
            }, false);
        })(this);
    };

    /**
     * When Borlabs is loaded, the consent will be verified. We need this, because the consent can be given before the jentis. consent engine was injected into the site
     */
    this.listenInitBorlabs = function () {
        (function (oMe) {
            if (typeof window.BorlabsCookie != "undefined") {
                oMe.borlabsReady = true;
                oMe.startTracking();
            } else {
                setTimeout(function () {
                    oMe.listenInitBorlabs();
                }, 500);
            }
        })(this);
    };
    
    this.listenConsentEngineShowAgain = function () {
        var oEventBaseObject;

        if(typeof window.jentis !== "undefined" && typeof window.jentis.consent !== "undefined" &&typeof window.jentis.consent.engine !== "undefined")
        {
            oEventBaseObject = window.jentis.consent.engine;
        }
        else
        {
            oEventBaseObject = document;
        }

        (function(oMe,oEventBaseObject){
            oEventBaseObject.addEventListener('jentis.consent.engine.show-bar',function(e)
            {
                oMe.startTracking();
            });

        })(this,oEventBaseObject);
    }

    this.getVendorValue = function(config, vendor, key) {
        if(typeof config[vendor] !== "undefined" && typeof config[vendor][key] !== "undefined") {
            return config[vendor][key];
        }
    
        return config.NOTCONFIGUREDVENDOR[key];
    }

    /**
     * Start Tracking
     *
     * When JENTIS Consent Engine and Usercentricks are ready we can start to send the consent information to the Engine.
     *
     */
    this.startTracking = function () {

        if (this.consentEngineReady && this.borlabsReady) {
            //Both APIs are ready.

            //Get all Vendor Data from the Engine to synchronizise with the Consent Descissions from Borlabs.
            var oJentisConsentEngineVendorData = window.jentis.consent.engine.getVendorFullData();

            var oNewVendorStatus = {};

            //Iterator through all vendors from the consent engine.
            for (var sVendorId in oJentisConsentEngineVendorData) {
                var oVendorObj = oJentisConsentEngineVendorData[sVendorId];

                var sBorlabsVendorID = this.getVendorValue(this.vendorConfig, sVendorId, "internalBorlabsID");

                //Check if the consentMode is consent. If not, we dont need any information from Usercentrics.
                if ((oVendorObj.consentMode === 0 || oVendorObj.consentMode === 2) && typeof sBorlabsVendorID != "undefined" && sBorlabsVendorID.length > 0) {

                    //Create the object with the status of the vendors regarding to their purposes
                    if (typeof window.BorlabsCookie.checkCookieConsent === 'function') {
                        oNewVendorStatus[sVendorId] = window.BorlabsCookie.checkCookieConsent(sBorlabsVendorID);
                    }else if(typeof window.BorlabsCookie.Consents !== "undefined" && typeof window.BorlabsCookie.Consents.hasConsent === "function") {
                        oNewVendorStatus[sVendorId] = window.BorlabsCookie.Consents.hasConsent(sBorlabsVendorID);
                    }
                }
            }
            //Now send the new vendor status to the Consent Engine.
            jentis.consent.engine.setNewVendorConsents(oNewVendorStatus);
        }
    };
    this.init();

};